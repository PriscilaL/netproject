﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class ReporteFactura
    {
        private String cod_factura;
        private DateTime fecha;
        private double subtotal;
        private double iva;
        private double total;
        private int cantidad;
        private double precio;
        private int SKU;
        private String nombre_producto;
        private String nombre_empleado;
        private String apellido_empleado;

        public ReporteFactura(string cod_factura, DateTime fecha, double subtotal, double iva, double total, int cantidad, double precio, int sKU, string nombre_producto, string nombre_empleado, string apellido_empleado)
        {
            this.cod_factura = cod_factura;
            this.fecha = fecha;
            this.subtotal = subtotal;
            this.iva = iva;
            this.total = total;
            this.cantidad = cantidad;
            this.precio = precio;
            SKU = sKU;
            this.nombre_producto = nombre_producto;
            this.nombre_empleado = nombre_empleado;
            this.apellido_empleado = apellido_empleado;
        }

        public string Cod_factura
        {
            get
            {
                return cod_factura;
            }

            set
            {
                cod_factura = value;
            }
        }

        public DateTime Fecha
        {
            get
            {
                return fecha;
            }

            set
            {
                fecha = value;
            }
        }

        public double Subtotal
        {
            get
            {
                return subtotal;
            }

            set
            {
                subtotal = value;
            }
        }

        public double Iva
        {
            get
            {
                return iva;
            }

            set
            {
                iva = value;
            }
        }

        public double Total
        {
            get
            {
                return total;
            }

            set
            {
                total = value;
            }
        }

        public int Cantidad
        {
            get
            {
                return cantidad;
            }

            set
            {
                cantidad = value;
            }
        }

        public double Precio
        {
            get
            {
                return precio;
            }

            set
            {
                precio = value;
            }
        }

        public int SKU1
        {
            get
            {
                return SKU;
            }

            set
            {
                SKU = value;
            }
        }

        public string Nombre_producto
        {
            get
            {
                return nombre_producto;
            }

            set
            {
                nombre_producto = value;
            }
        }

        public string Nombre_empleado
        {
            get
            {
                return nombre_empleado;
            }

            set
            {
                nombre_empleado = value;
            }
        }

        public string Apellido_empleado
        {
            get
            {
                return apellido_empleado;
            }

            set
            {
                apellido_empleado = value;
            }
        }
    }
}
